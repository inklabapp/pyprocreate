# pyprocreate

A package to parse and interact with Apple's Procreate (.pro) file format.

Currently only supports reading! But writing is in-scope for a future version. 

Documentation here: https://pyprocreate.readthedocs.io 

Please feel free to ask questions or report bugs and I will do my best to get back to you as soon as I am able. ^_^
