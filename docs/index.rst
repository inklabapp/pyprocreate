.. pyprocreate documentation master file, created by
   sphinx-quickstart on Thu Nov 14 12:56:52 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyprocreate's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage/quickstart

.. autoclass:: pyprocreate.Project
   :members:

.. autoclass:: pyprocreate.Layer
   :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
