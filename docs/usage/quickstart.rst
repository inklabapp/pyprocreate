Quickstart
=======================================

This is most likely what you are going to end up paying attention to. :)

General Info
=======================================
    - Currently only reading is supported, writing supported is planned for a future version.
    - Opacity values should always be 0.0 - 1.0
    - Blend / Clip layers are currently exported as regular image layers, leaving the blend / clip composition
      to be done externally. Automatic support for these operations in this library TBD.
    - Each layer is the same size as the whole project - contentsRect is not usually respected by procreate from my
      experiments. This is why the clip_to_content argument to get_image_data() is default False.

Reading Data
=======================================

Available data for reading includes basic metadata about the project and it's layers, as well as the Image data.

::

    from pyprocreate import Project

    project = Project("/path/to/some/project.procreate")  # open a project
    print(project.name)
    project.close()  # close handle on the procreate file

    with Project("/path/to/some/project.procreate") as project:  # also works as a context manager
        width, height = project.dimensions

        # layers can be referenced in order
        for layer in project.layers:
            print(layer.name, layer.dimensions, layer.bounding_rect, layer.UUID)
            print(layer.z_index, layer.opacity, layer.visible, layer.hidden)

        # or by name
        layer = project['Awesome Layer']

        # or by UUID
        layer = project.get_layer_by_uuid("6A163DC4-B344-4C76-AF3B-369484692B20")

You can get image data for the whole project, or each layer separately. Each image data will be returned as a
`Pillow/PIL Image() Object`_.

::

    project.get_image_data().save("/some/disk/location.png")  # PIL exports automatically based on the file ext you give

    for layer in project.layers:
        project.get_image_data().crop((0,0,50,50)).save(f"{layer.name}.png")  # all PIL methods work here











.. _Pillow/PIL Image() Object: https://pillow.readthedocs.io/en/stable/reference/Image.html